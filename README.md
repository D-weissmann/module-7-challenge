## Module 7 - Challenge

# Overview
    This is a look at the upcoming retirements from Packard Hewlet. There is an upcoming wave of employees who are eligible for retirement and this analysis goes to see how big of a problem its going to be. 
    
# Results
    Its a huge problem.
        1. Over 90 thousand people will be eligible for retirement in the near future
        2. Under 2 thousand people will be eligible for mentorship oportunities. 
        3. The majority of the potential retirees will be from higher up the chain of command and so it will be dificult to hire this level of worker vs the younger entry level jobs. 
        4. Even the Engineers and Staff will be hard hit and will likely be shortstaffed 

# Summary
    As stated this is a huge problem with no easy fix. Over 90 thousand employees will be able to retire in the near future with the number of workers who are eligible for a mentorship program being less than 2 thousand under the current rules. It might make sense to widen the parameters of the mentorship program and seeing if the program is capable of providing the number of workers required. 
