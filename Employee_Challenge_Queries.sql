Create Table Departments (
	dept_no Varchar(4) not null,
	dept_name Varchar(40) not null,
	Primary Key(dept_no),
	UNIQUE(dept_name)
	);
	
CREATE TABLE Employees(
	emp_no INT not null,
	birth_date DATE not null,
	first_name VARCHAR not null,
	last_name VARCHAR not null,
	gender VARCHAR not null,
	hire_date DATE not null,
	PRIMARY KEY(emp_no)
	);


CREATE TABLE dept_emp(
	emp_no INT not null,
	dept_no VARCHAR(4) not null,
	from_date DATE not null,
	to_date DATE not null,
	FOREIGN KEY(emp_no) REFERENCES Employees(emp_no),
	FOREIGN KEY(dept_no) REFERENCES Departments(dept_no),
	PRIMARY KEY(emp_no, dept_no, to_date)
	);
	
CREATE TABLE dept_manager(
	dept_no VARCHAR(4) not null,
	emp_no INT NOT null,
	from_date DATE NOT null,
	to_date DATE NOT null,
	FOREIGN KEY(dept_no) REFERENCES Departments(dept_no),
	FOREIGN KEY(emp_no) REFERENCES Employees(emp_no),
	PRIMARY KEY(dept_no, emp_no)
	);
	
CREATE TABLE salaries(
	emp_no INT NOT null,
	salary INT NOT null,
	from_date DATE NOT null,
	to_date DATE NOT null,
	FOREIGN KEY(emp_no) REFERENCES Employees(emp_no),
	PRIMARY KEY(emp_no)
	);

Drop Table titles

CREATE TABLE titles(
	emp_no INT NOT null,
	title VARCHAR NOT null,
	from_date DATE NOT null,
	to_date DATE NOT null,
	FOREIGN KEY(emp_no) REFERENCES Employees(emp_no),
	PRIMARY KEY(emp_no, title, to_date)
	);
	
SELECT employees.emp_no, 
	employees.first_name, 
	employees.last_name,
	titles.title, 
	titles.from_date, 
	titles.to_date
INTO retirement_info
FROM employees
Join titles
	ON employees.emp_no = titles.emp_no
WHERE employees.birth_date BETWEEN '1952-01-01' AND '1955-12-31'

select* from retirement_info

SELECT DISTINCT ON (emp_no) emp_no, 
	first_name,
	last_name,
	title
INTO unique_titles
FROM retirement_info
ORDER BY emp_no ASC, to_date DESC;

select * from unique_titles

drop table retiring_titles

SELECT COUNT(title), title
INTO retiring_titles
FROM unique_titles
GROUP BY title
ORDER BY (count) DESC;

select * from retiring_titles

SELECT DISTINCT ON(emp_no) employees.emp_no, 
	employees.first_name,
	employees.last_name,
	employees.birth_date,
	dept_emp.from_date,
	dept_emp.to_date,
	titles.title
INTO mentorship_availability
FROM employees
JOIN dept_emp
	ON employees.emp_no = dept_emp.emp_no
JOIN titles
	ON employees.emp_no = titles.emp_no
WHERE  (birth_date BETWEEN '1965-01-01' AND '1965-12-31')
ORDER BY emp_no ASC, to_date DESC;

SELECT * FROM mentorship_availability;