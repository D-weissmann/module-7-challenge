Create Table Departments (
	dept_no Varchar(4) not null,
	dept_name Varchar(40) not null,
	Primary Key(dept_no),
	UNIQUE(dept_name)
	);
	
CREATE TABLE Employees(
	emp_no INT not null,
	birth_date DATE not null,
	first_name VARCHAR not null,
	last_name VARCHAR not null,
	gender VARCHAR not null,
	hire_date DATE not null,
	PRIMARY KEY(emp_no)
	);
	
CREATE TABLE dept_emp(
	emp_no INT not null,
	dept_no VARCHAR(4) not null,
	from_date DATE not null,
	to_date DATE not null,
	FOREIGN KEY(emp_no) REFERENCES Employees(emp_no),
	FOREIGN KEY(dept_no) REFERENCES Departments(dept_no),
	PRIMARY KEY(emp_no)
	);
	
CREATE TABLE dept_manager(
	dept_no VARCHAR(4) not null,
	emp_no INT NOT null,
	from_date DATE NOT null,
	to_date DATE NOT null,
	FOREIGN KEY(dept_no) REFERENCES Departments(dept_no),
	FOREIGN KEY(emp_no) REFERENCES Employees(emp_no),
	PRIMARY KEY(dept_no, emp_no)
	);
	
CREATE TABLE salaries(
	emp_no INT NOT null,
	salary INT NOT null,
	from_date DATE NOT null,
	to_date DATE NOT null,
	FOREIGN KEY(emp_no) REFERENCES Employees(emp_no),
	PRIMARY KEY(emp_no)
	);
	
CREATE TABLE titles(
	emp_no INT NOT null,
	title VARCHAR NOT null,
	from_date DATE NOT null,
	to_date DATE NOT null,
	FOREIGN KEY(emp_no) REFERENCES Employees(emp_no),
	PRIMARY KEY(emp_no)
	);